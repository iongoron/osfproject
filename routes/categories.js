exports.categories = function(req, res) {
	var _         = require("underscore");
	var mdbClient = require('mongodb').MongoClient;
	var session = require('client-sessions');
	var id = req.params.id;
 	var baseURL = req.originalUrl +"/";
	
	mdbClient.connect("mongodb://localhost:27017/osf", function(err, db) {
		var collection = db.collection('shop');
		collection.find({"id": id}).toArray(function(err, items) {
			 var category = _.reduce(items, function(elem) {
				return elem.categories;
			});	
			res.render("categories", { 
				// Underscore.js lib
				_     : _, 
				// Template data
				title : items.name,
				page_description: category.page_description,
				items:category,
				baseURL:baseURL,
				id:id,
				session: req.session
			});
			db.close();
		});
	});
};