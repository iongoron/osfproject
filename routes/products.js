exports.products = function(req, res) {
	var _         = require("underscore");
	var mdbClient = require('mongodb').MongoClient;
	var http = require('http');
	var session = require('client-sessions');
	var productID = req.params.productID;
	var id = req.params.id;
	var categ = req.params.categ;
	var list = req.params.list;
  	var baseURL = req.originalUrl +"/";
	var date = '2017-03-20T00:00:00';
	var currency=[];
	var data =
		'<?xml version="1.0" encoding="utf-8"?>'+
		'<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">'+
		  '<soap:Body>'+
		    '<getall xmlns="http://www.infovalutar.ro/">'+
		      '<dt>'+date+'</dt>'+
		    '</getall>'+
		  '</soap:Body>'+
		'</soap:Envelope>'
	;

	var options = {
	  host: 'infovalutar.ro',
	  path: '/curs.asmx?wsdl',
	  method: 'POST',
	  headers: {
		'Content-Type': 'text/xml',
		'SOAPAction': "http://www.infovalutar.ro/getall",
	    'Content-Length': data.length,
	  }
	};
	var req1 = http.request(options, function(resp) {
	    var msg = '';
	    resp.on('data', function(chunk) {
	    	msg += chunk;
	    });
	    resp.on('end', function() {
			var msg1 = msg.split('<IDMoneda>');
			msg1.shift();
			_.each(msg1, function (row){
				var str1 = row.substring(0,3);
				var str2 = row.substring(21,26);
				currency.push([[str1],[str2]]);
	        });
	        	
			mdbClient.connect("mongodb://localhost:27017/osf", function(err, db) {
				var collection = db.collection('prod');

				collection.find({"id": productID}).toArray(function(err, items) {
					var lists = _.reduce(items, function(elem) {
						   return elem;
						
					});
					res.render("product", { 
						// Underscore.js lib
						_     : _, 
						// Template data
						title :lists.page_title,
						items:lists,
						baseURL:baseURL,
						id:id,
						categ:categ,
						list:list,
						product:lists.name,
						currency: currency,
						session: req.session,
						productID: productID
					});

					db.close();
				});
			});



		});
	});

	req1.write(data);    

	req1.end();
};