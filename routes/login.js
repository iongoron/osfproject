exports.login = function(req, resp) {
	var _         = require("underscore");
	var mdbClient = require('mongodb').MongoClient;
	var bcrypt = require('bcrypt');
	var session = require('client-sessions');
	
	mdbClient.connect("mongodb://localhost:27017/osf", function(err, db) {
		var collection = db.collection('User');
		  collection.findOne({ email: req.body.email }, function(err, user) {
		    if (!user) {
		      resp.redirect('/');
		    } else {
				bcrypt.compare(req.body.password, user.password, function(err, res) {
				  if(res) {
				  	req.session.user = user;		        	  
					resp.redirect('/');
				   console.log('Passwords match');
				  } else {
				  	resp.redirect('/');
				   console.log('Passwords dont match');
				  } 
				});
		    }
		  });
	});
};