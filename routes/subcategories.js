exports.subcategories = function(req, res) {
	var _         = require("underscore");
	var mdbClient = require('mongodb').MongoClient;
	var session = require('client-sessions');
	var id = req.params.id;
	var categ = req.params.categ;
 	var baseURL = req.originalUrl +"/";

	mdbClient.connect("mongodb://localhost:27017/osf", function(err, db) {
		var collection = db.collection('shop');
		collection.find({"id": id}).toArray(function(err, items) {
			var category;
			_.each(items, function(topC) { 
             	_.each(topC.categories, function(subC) { 
                        if (subC.id== categ){
                        	category = subC;
                        }	
                });                           
            }); 
			res.render("categories", { 
				// Underscore.js lib
				_     : _, 
				// Template data
				title : category.name,
				page_description: category.page_description,
				items:category,
				baseURL:baseURL,
				id:id,
				categ:categ,
				session: req.session
			});
			db.close();
		});
	});
};