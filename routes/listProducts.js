exports.listProducts = function(req, res) {
	var _         = require("underscore");
	var mdbClient = require('mongodb').MongoClient;
	var session = require('client-sessions');
	var id = req.params.id;
	var categ = req.params.categ;
	var list = req.params.list;
 	var baseURL = req.originalUrl +"/";

	mdbClient.connect("mongodb://localhost:27017/osf", function(err, db) {
		var collection = db.collection('prod');
		collection.find({"primary_category_id": list}).toArray(function(err, items) {
			var lists = _.reduce(items, function(elem) {
				return elem;
			});
			if (lists!==undefined) {
				res.render("productList", { 
					// Underscore.js lib
					_     : _, 
					// Template data
					title :lists.page_title,
					items:items,
					baseURL:baseURL,
					id:id,
					categ:categ,
					list:list,
					currency: "$",
					session: req.session
				});
			}else{
				URL = "http://localhost:8080/shop/"+id+"/"+categ;
				res.render("noProduct", {
					_:_,
					list:list,
					baseURL:URL,
					session: req.session
				})
			}
			db.close();
		});
	});
};