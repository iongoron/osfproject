exports.addToList = function(req, res) {
	var _         = require("underscore");
	var mdbClient = require('mongodb').MongoClient;
	var product = req.params.product;
	mdbClient.connect("mongodb://localhost:27017/osf", function(err, db) {
		var collection = db.collection('User');
		try {
		collection.findOneAndUpdate(
		   { "email" : req.session.user.email },
		   { $addToSet: { "product" : product}}
		);
		backURL=req.header('Referer') || '/';
				    res.redirect(backURL);
		}
		catch (e){
		   console.log(e);
		}
		db.close();
	});
};