exports.index = function(req, res) {
	var _         = require("underscore");
	var mdbClient = require('mongodb').MongoClient;
 	var baseURL = req.originalUrl +"/";
	var session = require('client-sessions');
	
	mdbClient.connect("mongodb://localhost:27017/osf", function(err, db) {
		var collection = db.collection('shop');
		collection.find().toArray(function(err, items) {
			res.render("index", { 
				// Underscore.js lib
				_     : _, 
				
				// Template data
				title : items.name,
				items:items,
				baseURL:baseURL,
				session: req.session,
			});
			db.close();
		});
	});
};