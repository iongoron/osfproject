/* global params */

// Module dependencies.
var express    = require("express")
  , http       = require("http")
  , path       = require("path")
  , routes     = require("./routes")
  , index      = require('./routes/index')
  , signupPage = require('./routes/signupPage')
  , loginPage  = require('./routes/loginPage')
  , categories= require('./routes/categories')
  , subcategories=require('./routes/subcategories')
  , listProducts=require('./routes/listProducts')
  , products = require('./routes/products')
  ,signup=require('./routes/signup')
  , login = require('./routes/login')
  , logout = require('./routes/logout')
  , dashboard = require('./routes/dashboard')
  , addToList = require('./routes/addToList')
  , session    = require('client-sessions');

var app     = express();

// All environments
app.set("port", 8080);
app.set("views", __dirname + "/views");
app.set("view engine", "ejs");
app.use(express.favicon());
app.use(express.logger("dev"));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.cookieParser("61d333a8-6325-4506-96e7-a180035cc26f"));
app.use(express.session());
app.use(app.router);
app.use(express.static(path.join(__dirname, "public")));
app.use(express.errorHandler());
app.use(express.static('assets'));
app.use(session({
  cookieName: 'session',
  secret: 'my$hop',
  duration: 30 * 60 * 1000,
  activeDuration: 5 * 60 * 1000,
}));
// App routes
app.get("/"     			, index.index);
app.get("/signupPage"       , signupPage.signupPage);
app.get("/loginPage"        , loginPage.loginPage);
app.get("/shop/:id"         , categories.categories);
app.get("/shop/:id/:categ"  , subcategories.subcategories);
app.get("/shop/:id/:categ/:list", listProducts.listProducts);
app.get("/shop/:id/:categ/:list/:productID", products.products);
app.post("/signup"          , signup.signup);
app.post("/login"     		, login.login);
app.get("/logout"     		, logout.logout);
app.get('/dashboard'		, dashboard.dashboard);
app.get('/add/:product'		, addToList.addToList);
// Run server
http.createServer(app).listen(app.get("port"), function() {
	console.log("Express server listening on port " + app.get("port"));
});
